var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyParser = require('body-parser');
app.use(bodyParser.json());

var movimientosJSON = require('./movimientosv2.json');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.get('/Clientes/:idcliente', function(req, res){
  res.send('Aquí tiene al cliente número: ' + req.params.idcliente);
})

app.post('/', function(req, res){
  res.send('Hemos recibido su petición post');
})

app.put('/', function(req, res){
  res.send('Hemos recibido su petición put cambiada');
})

app.delete('/', function(req, res){
  res.send('Hemos recibido su petición delete');
})

app.get('/v1/movimientos', function(req, res){
  res.sendfile('movimientosv1.json');
})

app.get('/v2/movimientos', function(req, res){
  res.json(movimientosJSON);
})

app.get('/v2/movimientos/:indice', function(req, res){
  console.log(req.params.indice);
  res.send(movimientosJSON[req.params.indice]);
})

app.get('/v3/movimientosquery', function(req, res){
  console.log(req.query);
  res.send('Recibido');
})

app.post('/v3/movimientos', function(req, res){
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send('Movimiento dado de alta')
})
